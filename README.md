# Embedded Systems Project

This repo contains the code and documentation of the project realized for the Embedded Systems course, developed during my Master's Degree course in Computer Engineering.

# MoonWalk - Smart Insole

Since the STM32_MoonWalk project is the integration of two codes, the one of the Wi-Fi module and the one of the STM32F401RE, it was necessary to link files inside the folder.
Therefore, before to build the project, the link for the following files must be deleted and re-added "main.c", "stm32_xx_it.c" and "stm32f4xx_hal_adc.c".

From the project opened in the Eclipse IDE, open the "drivers" folder -> right click -> new -> file -> advanced -> link into the file system and add the file link with the path: "/gruppo_1/MoonWalk/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc.c".

Same procedure for the other two:
"/gruppo_1/MoonWalk/Projects/Multi/Applications/Server_Socket/Src/main.c"
"/gruppo_1/MoonWalk/Projects/Multi/Applications/Server_Socket/Src/stm32_xx_it.c"

The project called "STM32_MoonWalk" contains the source code, written in C language, loaded on the STM32 microcontroller.
The project called "MoonWalk" contains the scripts, written in Python language, related to the notebook application for walking and posture monitoring.
