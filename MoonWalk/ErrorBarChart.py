import pandas as pd
from matplotlib.figure import Figure


class ErrorBarChart:

    def __init__(self, occWList, occAList):
        self.fig = Figure()
        self.ax = self.fig.add_subplot(111)
        self.errorList = ['Tip', 'Heel', 'Left', 'Right']
        self.occWList = occWList
        self.occAList = occAList

        dict = {
            'Error': self.errorList,
            'OccW': self.occWList,
            'OccA': self.occAList
        }

        self.df = pd.DataFrame(dict)

        self.new_df = self.df[['Error', 'OccW', 'OccA']].groupby('Error').sum()
        self.new_df.plot(kind='bar', legend=True, ax=self.ax)

    def update(self, occWList, occAList):

        self.occWList = occWList
        self.occAList = occAList
        self.df = pd.DataFrame({
            'Error': self.errorList,
            'OccW': self.occWList,
            'OccA': self.occAList
        })
        self.new_df = self.df[['Error', 'OccW', 'OccA']].groupby('Error').sum()
        self.new_df.plot(kind='bar', legend=False, ax=self.ax)
