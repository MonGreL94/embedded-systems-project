from Tkinter import *
from PIL import ImageTk, Image
import matplotlib
import HeatMap as ht
import ErrorBarChart as eb
import socket
import select
import time
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk

h = ht.HeatMap(1, 1, 1)
fig1 = h.fig
im1 = h.im

bar = eb.ErrorBarChart([0, 0, 0, 0], [0, 0, 0, 0])
fig2 = bar.fig
a2 = bar.ax


class App(Tk):

    def __init__(self, *args, **kwargs):
        Tk.__init__(self, *args, **kwargs)
        Tk.wm_title(self,"Welcome to Team1's App!")

        container = Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (PageOne, PageTwo):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(PageOne)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()


class PageOne(Frame):

    def __init__(self, parent, controller):
        Frame.__init__(self, parent)

        self.configure(bg='black')

        img0 = Image.open("logo.jpg")
        l0 = Label(self)
        l0.img = ImageTk.PhotoImage(img0)
        l0["image"] = l0.img
        l0.pack()

        def click0():
            controller.show_frame(PageTwo)

        b0 = Button(self, text="Start", command=click0, width=10)
        b0.pack()


class PageTwo(Frame):

    def __init__(self,parent, controller):
        Frame.__init__(self,parent)
        self.remaining = 0

        topframe = Frame(self)
        topframe.pack(side=TOP, fill=X)

        outputframe = Frame(self)
        outputframe.pack(side=TOP, fill=X)

        graphframe = Frame(self)
        graphframe.pack(side=TOP)

        bottomframe = Frame(self)
        bottomframe.pack(side=BOTTOM)

        #TOP

        def click0():

            l2["text"] = ""
            l5["text"] = ""
            l7["text"] = ""
            b0["state"] = DISABLED

            h.update(1, 1, 1, 1)
            bar.ax.clear()


            try:
                l3["text"] = "Connecting..."

                if t0.get() == '':
                    min = 0
                else:
                    min = int(t0.get())

                if t1.get() == '':
                    sec = 0
                else:
                    sec = int(t1.get())

                time1 = time.time() + 60*min + sec

                HOST = "192.168.43.70"
                PORT = 32000
                server_address = (HOST, PORT)
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sock.connect(server_address)
                l3["text"] = "Connected!"

                data = None
                dataList = []
                old_len = 0
                t_prev_e = time.time()
                prev_e = None
                err_tot, err0, err1, err2, err3, err4, err5, err6, err7, errp1, errp2, errp3 = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

                sock.setblocking(0)

                while time.time() <= time1:

                    ready = select.select([sock], [], [], 0.1)
                    if ready[0]:
                        data = sock.recv(1000).decode("UTF-8")
                        dataList.append(data)

                    new_len = len(dataList)

                    if new_len > 1 and old_len < new_len:
                        curr_e = data
                        t_curr_e = time.time()

                        if curr_e != prev_e or (curr_e == prev_e and (t_curr_e - t_prev_e >= 0.8)):

                            err_tot += 1

                            if data == "0" or data == "1":
                                err0 += 1
                                errp2 += 1
                                errp3 += 1

                                h.update(err_tot, errp1, errp2, errp3)

                                if data == "0":
                                    l5["text"] = "You started the step with the tip!"

                                if data == "1":
                                    l5["text"] = "Too much time on the tip!"

                            if data == "2":
                                err1 += 1
                                errp1 += 1

                                h.update(err_tot, errp1, errp2, errp3)

                                l5["text"] = "Too much time on the heel!"

                            if data == "3":
                                err2 += 1
                                errp1 += 1
                                errp2 += 1

                                h.update(err_tot, errp1, errp2, errp3)

                                l5["text"] = "All the weight on the left!"

                            if data == "4":
                                err3 += 1
                                errp1 += 1
                                errp3 += 1

                                h.update(err_tot, errp1, errp2, errp3)

                                l5["text"] = "All the weight on the right!"

                            if data == "5":
                                err4 += 1

                                h.update(err_tot, errp1, errp2, errp3)

                                l5["text"] = "Dangerous inclination of the tip!"

                            if data == "6":
                                err5 += 1

                                l5["text"] = "Dangerous inclination of the heel!"

                            if data == "7":
                                err6 += 1

                                l5["text"] = "Dangerous inclination on the left!"

                            if data == "8":
                                err7 += 1

                                l5["text"] = "Dangerous inclination on the right!"

                            t_prev_e = t_curr_e
                            prev_e = curr_e

                            bar.update([err0, err1, err2, err3], [err4, err5, err6, err7])

                    old_len = len(dataList)

                    canvas1.draw()
                    canvas2.draw()

                    app.update_idletasks()
                    app.update()

                sock.close()

                b0["state"] = ACTIVE
                l2["text"] = "Time's Up!"

                def report(err_tot, err0, err1, err2, err3):
                    text = ""

                    if err_tot == 0:
                        text = " No errors have been detected: you have a perfect walk! "

                    if err3 == max(err0, err1, err2, err3) and err3 > err2:
                        text = text + str(err3) + " times, you spread the weight incorrectly on the right! Possible risk: pronation of the foot. "

                    if err2 == max(err0, err1, err2, err3) and err2 > err3:
                        text = text + str(err2) + " times, you spread the weight incorrectly on the left! Possible risk: supination of the foot. "

                    if err0 == max(err0, err1, err2, err3) and err0 > err1:
                        text = text + " Frequently you spread the weight incorrectly on the tip! "

                    if err1 == max(err0, err1, err2, err3) and err1 > err0:
                        text = text + " Frequently you spread the weight incorrectly on the heel! "

                    l7["text"] = text

                report(err_tot, err0, err1, err2, err3)

            except ValueError:
                l2["text"] = "Uncorrect Value!"
                l3["text"] = ""
                b0["state"] = ACTIVE

            except IndexError:
                pass

            except TclError:
                pass

        l0 = Label(topframe, text="Scan Time:", font=("Arial Bold", 19))
        l0.grid(row=0, column=0, sticky=W)

        l6 = Label(topframe, text="min:", font=("Arial Bold", 16))
        l6.grid(row=1, column=0, sticky=E)

        t0 = Entry(topframe, width=10)
        t0.grid(row=1, column=1, sticky=W)

        l1 = Label(topframe, text="sec:", font=("Arial Bold", 16))
        l1.grid(row=1, column=2, sticky=W)

        t1 = Entry(topframe, width=10)
        t1.grid(row=1, column=3, sticky=W)

        b0 = Button(topframe, text="Connect&Start", command=click0)
        b0.grid(row=1, column=4, sticky=W)

        l2 = Label(topframe, text="", font=("Arial Bold", 19), width=15)
        l2.state = True
        l2.grid(row=1, column=6, sticky=W)

        l3 = Label(topframe, text="", font=("Arial Bold", 19))
        l3.grid(row=1, column=7, sticky=W)

        l4 = Label(topframe, text="Output:", font=("Arial Bold", 19))
        l4.grid(row=2, column=0, sticky=W)

        #OUTPUT FRAME

        l5 = Label(outputframe, text="", font=("Arial Bold", 25))
        l5.pack()

        #GRAPH FRAME

        canvas1 = FigureCanvasTkAgg(fig1, graphframe)
        fig1.set_canvas(canvas1)
        canvas1.draw()

        toolbar1 = NavigationToolbar2Tk(canvas1, self)
        toolbar1.pack(side=LEFT)
        toolbar1.update()
        canvas1._tkcanvas.pack(side=LEFT, fill=BOTH)
        canvas1.get_tk_widget().pack(side=LEFT, fill=BOTH)

        canvas2 = FigureCanvasTkAgg(fig2, graphframe)
        fig2.set_canvas(canvas2)
        canvas2.draw()

        toolbar2 = NavigationToolbar2Tk(canvas2, self)
        toolbar2.pack(side=RIGHT)
        toolbar2.update()
        canvas2._tkcanvas.pack(side=LEFT, fill=BOTH)
        canvas2.get_tk_widget().pack(side=LEFT, fill=BOTH)

        #BOTTOM

        l7 = Label(bottomframe, font=("Arial Bold", 19))
        l7.pack(fill=X)


app = App()
app.mainloop()
