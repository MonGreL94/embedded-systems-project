from __future__ import division
import numpy as np
import matplotlib.patches as patches
from matplotlib.path import Path
from matplotlib.figure import Figure


def matrix(p1, p2, p3):
    grid = np.array([(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, p1, p1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, p1, p1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, p2, p2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, p2, p2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, p3, p3, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, p3, p3, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                     (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)])
    return grid


class HeatMap:

    def __init__(self, p1, p2, p3):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3

        self.fig = Figure()
        self.ax = self.fig.add_subplot(111)

        foot_verts = [(4.9, 22),
                      (4.3, 16.3), (6.7, 12.7), (6.5, 11),
                      (6.7, 9), (6.3, 5.4), (7.3, 3.3),
                      (8.5, 0.5), (13.1, 1.5), (14.1, 4),
                      (15.1, 6.6), (13.9, 9.3), (13.5, 10.9),
                      (13.3, 14.8), (15.7, 16.6), (14.9, 24.4),
                      (13.9, 31.9), (5.5, 27.7), (4.9, 22)]

        foot_codes = [Path.MOVETO,
                      Path.CURVE4, Path.CURVE4, Path.CURVE4,
                      Path.CURVE4, Path.CURVE4, Path.CURVE4,
                      Path.CURVE4, Path.CURVE4, Path.CURVE4,
                      Path.CURVE4, Path.CURVE4, Path.CURVE4,
                      Path.CURVE4, Path.CURVE4, Path.CURVE4,
                      Path.CURVE4, Path.CURVE4, Path.CURVE4]

        path = Path(foot_verts, foot_codes)
        patch = patches.PathPatch(path, facecolor='none', edgecolor='black', lw=1)
        self.ax.add_patch(patch)

        self.im = self.ax.imshow(matrix(self.p1, self.p2, self.p3), clip_path=patch, clip_on=True, cmap='viridis',
                       interpolation='spline36')

        self.ax.set_xlim(0, 20)
        self.ax.set_ylim(0, 30)
        self.ax.set_label('Foot\'s Heatmap')
        self.ax.set_ylabel('y (cm)')
        self.ax.set_xlabel('x (cm)')
        cb = self.fig.colorbar(self.im)
        cb.set_label('Weight Distribution')

    def update(self, tot, errp1, errp2, errp3):

        self.p1 = errp1 / tot
        self.p2 = errp2 / tot
        self.p3 = errp3 / tot

        self.im.set_array(matrix(self.p1, self.p2, self.p3))