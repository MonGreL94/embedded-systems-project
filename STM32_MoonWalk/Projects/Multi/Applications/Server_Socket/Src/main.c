  /**
  ******************************************************************************
  * @file    main.c
  * @author  Central LAB
  * @version V2.1.0
  * @date    17-May-2015
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2015 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "string.h"
#include "stdio.h"
#include "string.h"
#include "wifi_module.h"
#include "wifi_globals.h"
#include "wifi_interface.h"

/** @defgroup WIFI_Examples
  * @{
  */

/** @defgroup WIFI_Example_Server_Socket
  * @{
  */
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
volatile uint8_t endConv = 0;
int val = 0;
int tab[71];
float avg1 = 0;
float avg2 = 0;
float avg3 = 0;
float avgx = 0;
float avgy = 0;
float avgz = 0;
int tallone = -1;
int punta = -1;
int inizio = -1;
float soglia1 = 0;
float soglia2 = 0;
float soglia3 = 0;
int tar = 1;
int i = 0;
int j = 0;
int tempo_tallone=0;
int tempo_punta=0;
uint16_t sens_act[6];
int fermo = 0;

float xAccel = 0;
float yAccel = 0;
float zAccel = 0;

#define OFFSET 200
#define MAX 3200
#define MAXDEFAULT 3000
#define LETTURE 1000
#define SIDE 500
#define CONT 100

#define XRAWMIN 1370
#define XRAWMAX 1985

#define YRAWMIN 1330
#define YRAWMAX 1965

#define ZRAWMIN 1370
#define ZRAWMAX 1995

#define AXTHRESHOLD 0.6

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
char print_msg_buff[512];

/* Private function prototypes2 -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);

/* Private functions ---------------------------------------------------------*/                   
void UART_Msg_Gpio_Init(void);
void USART_PRINT_MSG_Configuration(UART_HandleTypeDef *UART_MsgHandle, uint32_t baud_rate);
WiFi_Status_t wifi_get_AP_settings(void);

float map(float, float, float, int, int);
void leggiSensori(void);
void taratura(void);
void scriviAttuatori(GPIO_PinState, GPIO_PinState, GPIO_PinState, GPIO_PinState, GPIO_PinState);
void controlloPostura(void);

/* Private Declarartion ------------------------------------------------------*/
wifi_state_t wifi_state;
wifi_config config;
UART_HandleTypeDef UART_MsgHandle;

uint8_t console_input[1], console_count = 0;
char console_ssid[40];
char console_psk[20];

char * ssid = "HUAWEI P8 lite";
char * seckey = "federica";
uint8_t channel_num = 6;
WiFi_Priv_Mode mode = WPA_Personal;     
char echo[1][1000];
char ip_addr[16];
char mac_addr[17];
uint16_t len;
uint8_t sock_id, server_id;

char * gcfg_key1 = "ip_ipaddr";
char * gcfg_key2 = "nv_model";
uint8_t socket_id;
char wifi_ip_addr[20];
uint16_t len;
uint32_t baud_rate = 115200;

 /**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
  WiFi_Status_t status = WiFi_MODULE_SUCCESS;
  char *protocol = "t";
  uint32_t portnumber = 32000;
  
  __GPIOA_CLK_ENABLE();
  HAL_Init();

  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  HAL_DMA_Init(&hdma_adc1);

  /* Configure the system clock to 64 MHz */
  SystemClock_Config();

  /* configure the timers  */
  Timer_Config( );
  
#ifdef USART_PRINT_MSG
  UART_Msg_Gpio_Init();
  USART_PRINT_MSG_Configuration(&UART_MsgHandle, 115200);
  Set_UartMsgHandle(&UART_MsgHandle);
#endif  
  
  status = wifi_get_AP_settings();

  if(status != WiFi_MODULE_SUCCESS) {

    printf("\r\nError in AP Settings");
    return 0;

  }
  
  config.power = wifi_active;
  config.power_level = high;
  config.dhcp = on; //use DHCP IP address  
  config.mcu_baud_rate = baud_rate;
  wifi_state = wifi_state_idle;

  UART_Configuration(baud_rate); 

  printf("\r\nInitializing the wifi module...\r\n");


  /* Init the wi-fi module */  
  status = wifi_init(&config);

  if(status != WiFi_MODULE_SUCCESS) {

    printf("Error in Config");
    return 0;

  }
  
  printf("\r\nInitializing complete.\r\n");
  
  scriviAttuatori(GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_RESET);

  while (1) {

	  if (tar == 1) {

		  taratura();
		  
	  }

	  tar = 0;

	  leggiSensori();

	  controlloPostura();

	  
	  switch (wifi_state) {

	  case wifi_state_reset:

		  break;

      case wifi_state_ready:

        

        wifi_connect(ssid,seckey, mode);

        wifi_state = wifi_state_idle;

        break;

      case wifi_state_connected:
        
			  printf("\r\n >>connected...\r\n");

			  WiFi_Status_t status;

			  status = wifi_get_IP_address((uint8_t*)wifi_ip_addr);


			  memset(wifi_ip_addr, 0x00, 20);

			  status = wifi_get_MAC_address((uint8_t*)wifi_ip_addr);



			  wifi_state = wifi_state_socket;

    	  break;

      case wifi_state_disconnected:


    	  wifi_state = wifi_state_idle;

    	  break;

      case wifi_state_socket:



		  /* Read Write Socket data */
    	  status = wifi_socket_server_open(portnumber, (uint8_t *)protocol);

    	  if(status == WiFi_MODULE_SUCCESS) {


			  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_SET, GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_SET);
			  HAL_Delay(100);
			  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_SET, GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET);
			  HAL_Delay(100);
			  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_SET, GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_SET);
			  HAL_Delay(100);
			  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_SET, GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET);
			  HAL_Delay(100);

    	  }

    	  wifi_state = wifi_state_idle;

    	  break;

      case wifi_state_socket_write:


    	  /* Read Write Socket data */

    	  len = strlen(echo);
          status = wifi_socket_server_write(len, echo);

          if(status == WiFi_NOT_READY){
        	  
        	  wifi_state = wifi_state_idle;
        	  
        	  break;

          }

          if(status == WiFi_MODULE_SUCCESS) {


          }

          wifi_state = wifi_state_idle;

          break;

      case wifi_state_idle:


    	  break;

      default:

    	  break;

	  }

  }

}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSI)
  *            SYSCLK(Hz)                     = 64000000
  *            HCLK(Hz)                       = 64000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 2
  *            APB2 Prescaler                 = 1
  *            PLLMUL                         = 16
  *            Flash Latency(WS)              = 2
  * @param  None
  * @retval None
  */


#ifdef USE_STM32F4XX_NUCLEO

void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable Power Control clock */
  __PWR_CLK_ENABLE();
  
  /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
  
  /* Enable HSI Oscillator and activate PLL with HSI as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 0x10;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);
   
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;  
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);
}
#endif



#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

#ifdef USART_PRINT_MSG
void USART_PRINT_MSG_Configuration(UART_HandleTypeDef *UART_MsgHandle, uint32_t baud_rate)
{
  UART_MsgHandle->Instance             = WIFI_UART_MSG;
  UART_MsgHandle->Init.BaudRate        = baud_rate;
  UART_MsgHandle->Init.WordLength      = UART_WORDLENGTH_8B;
  UART_MsgHandle->Init.StopBits        = UART_STOPBITS_1;
  UART_MsgHandle->Init.Parity          = UART_PARITY_NONE ;
  UART_MsgHandle->Init.HwFlowCtl       = UART_HWCONTROL_NONE;// USART_HardwareFlowControl_RTS_CTS;
  UART_MsgHandle->Init.Mode            = UART_MODE_TX_RX;

  if(HAL_UART_DeInit(UART_MsgHandle) != HAL_OK)
  {
    Error_Handler();
  }  
  if(HAL_UART_Init(UART_MsgHandle) != HAL_OK)
  {
    Error_Handler();
  }
      
}

void UART_Msg_Gpio_Init()
{ 
  GPIO_InitTypeDef  GPIO_InitStruct;

  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable GPIO TX/RX clock */
  USARTx_PRINT_TX_GPIO_CLK_ENABLE();
  USARTx_PRINT_RX_GPIO_CLK_ENABLE();


  /* Enable USARTx clock */
  USARTx_PRINT_CLK_ENABLE(); 
    __SYSCFG_CLK_ENABLE();
  /*##-2- Configure peripheral GPIO ##########################################*/  
  /* UART TX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = WiFi_USART_PRINT_TX_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
#if defined (USE_STM32L0XX_NUCLEO) || defined(USE_STM32F4XX_NUCLEO) || defined(USE_STM32L4XX_NUCLEO)
  GPIO_InitStruct.Alternate = PRINTMSG_USARTx_TX_AF;
#endif  
  HAL_GPIO_Init(WiFi_USART_PRINT_TX_GPIO_PORT, &GPIO_InitStruct);

  /* UART RX GPIO pin configuration  */
  GPIO_InitStruct.Pin = WiFi_USART_PRINT_RX_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
#if defined (USE_STM32L0XX_NUCLEO) || defined(USE_STM32F4XX_NUCLEO) || defined(USE_STM32L4XX_NUCLEO)
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Alternate = PRINTMSG_USARTx_RX_AF;
#endif 
  
  HAL_GPIO_Init(WiFi_USART_PRINT_RX_GPIO_PORT, &GPIO_InitStruct);
  
#ifdef WIFI_USE_VCOM
  /*##-3- Configure the NVIC for UART ########################################*/
  /* NVIC for USART */
  HAL_NVIC_SetPriority(USARTx_PRINT_IRQn, 0, 1);
  HAL_NVIC_EnableIRQ(USARTx_PRINT_IRQn);
#endif
}
#endif  // end of USART_PRINT_MSG


/**
  * @brief  Query the User for SSID, password and encryption mode
  * @param  None
  * @retval WiFi_Status_t
  */
WiFi_Status_t wifi_get_AP_settings(void)
{
  WiFi_Status_t status = WiFi_MODULE_SUCCESS;
  printf("\r\n\n/********************************************************\n");
  printf("\r *                                                      *\n");
  printf("\r * X-CUBE-WIFI1 Expansion Software V3.1.1               *\n");
  printf("\r * X-NUCLEO-IDW0xx1 Wi-Fi Mini-AP Configuration.        *\n");
  printf("\r * Server-Socket Example                                *\n");
  printf("\r *                                                      *\n");
  printf("\r *******************************************************/\n");
  //printf("\r\nDo you want to setup SSID?(y/n):");
  //fflush(stdout);
  //scanf("%s",console_input);
  console_input[0]='n';
  
  printf("\r\n\nModule will connect with default settings.");
  memcpy(console_ssid, (const char*)ssid, strlen((char*)ssid));
  memcpy(console_psk, (const char*)seckey, strlen((char*)seckey));

  printf("\r\n/*************************************************************\r\n");
  printf("\r\n * Configuration Complete                                     \r\n");
  printf("\r\n * Please make sure a server is listening at given hostname   \r\n");
  printf("\r\n *************************************************************\r\n");

  return status;
  
}

/******** Wi-Fi Indication User Callback *********/

void ind_wifi_socket_data_received(int8_t callback_server_id, int8_t socket_id, uint8_t * data_ptr, uint32_t message_size, uint32_t chunk_size, WiFi_Socket_t socket_type)
{
  printf("\r\nData Receive Callback...\r\n");
  memcpy(echo, data_ptr, 50);
  printf((const char*)echo);
  printf("\r\nsocket ID: %d\r\n",socket_id);
  printf("msg size: %lu\r\n",(unsigned long)message_size);
  printf("chunk size: %lu\r\n",(unsigned long)chunk_size);
  fflush(stdout);
  sock_id = socket_id;//client_ID from where message has arrived
  server_id = callback_server_id;//server_ID from where message has arrived
  wifi_state = wifi_state_socket_write;
}

void ind_wifi_on()
{
    wifi_state = wifi_state_ready;
}

void ind_wifi_connected()
{
  wifi_state = wifi_state_connected;
}

void ind_socket_server_client_joined(void)
{
  printf("\r\nUser callback: Client joined...\r\n");
  fflush(stdout);
  sprintf(echo, "%f;%f;%f", soglia1, soglia2, soglia3);
  wifi_state = wifi_state_socket_write;

}

void ind_socket_server_client_left(void)
{
  printf("\r\nUser callback: Client left...\r\n");
  fflush(stdout);
}

void ind_wifi_ap_client_joined(uint8_t * client_mac_address)
{
  printf("\r\n>>client joined callback...\r\n");
  printf(">>client MAC address: ");
  printf((const char*)client_mac_address);
  fflush(stdout);  
}

void ind_wifi_ap_client_left(uint8_t * client_mac_address)
{
  printf("\r\n>>client left callback...\r\n");
  printf("\r\n >>client MAC address: ");
  printf((const char*)client_mac_address);
  fflush(stdout);  
}

void ind_wifi_error(WiFi_Status_t error_code)
{
  if(error_code == WiFi_AT_CMD_RESP_ERROR)
  {
    wifi_state = wifi_state_idle;
    printf("\r\n WiFi Command Failed. \r\n User should now press the RESET Button(B2). \r\n");
  }
}

//FROM SENSORS

/* ADC1 init function */
static void MX_ADC1_Init(void)
{
  ADC_ChannelConfTypeDef sConfig;

    /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ENABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 6;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    //_Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
    */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    //_Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
    */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = 2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    //_Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
    */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = 3;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    //_Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
    */
  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = 4;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    //_Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
    */
  sConfig.Channel = ADC_CHANNEL_10;
  sConfig.Rank = 5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    //_Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
    */
  sConfig.Channel = ADC_CHANNEL_11;
  sConfig.Rank = 6;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    //_Error_Handler(__FILE__, __LINE__);
  }

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);

}

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_RESET);

  /*Configure GPIO pins : PA5 PA6 PA7 */
  GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PB10*/
  GPIO_InitStruct.Pin = GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc) {

	endConv = 1;

}

float map(float x, float in_min, float in_max, int out_min, int out_max) {

	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;

}

void leggiSensori(void) {

	avg1 = 0;
	avg2 = 0;
	avg3 = 0;
	avgx = 0;
	avgy = 0;
	avgz = 0;

	for (i = 0; i < LETTURE; i++) {

		HAL_ADC_Start_DMA(&hadc1, (uint32_t*)sens_act, 6);
		while(!endConv);
		endConv = 0;

		avg1 += ((float) sens_act[0]) / 4095 * 3300;
		avg2 += ((float) sens_act[1]) / 4095 * 3300;
		avg3 += ((float) sens_act[2]) / 4095 * 3300;
		avgx += ((float) sens_act[3]) / 4095 * 3300;
		avgy += ((float) sens_act[4]) / 4095 * 3300;
		avgz += ((float) sens_act[5]) / 4095 * 3300;
	}

	avg1 = avg1/LETTURE;
	avg2 = avg2/LETTURE;
	avg3 = avg3/LETTURE;
	avgx = avgx/LETTURE;
	avgy = avgy/LETTURE;
	avgz = avgz/LETTURE;
	
	xAccel = (map(avgx, XRAWMIN, XRAWMAX, -10000, 10000)) / 10000;
	yAccel = (map(avgy, YRAWMIN, YRAWMAX, -10000, 10000)) / 10000;
	zAccel = (map(avgz, ZRAWMIN, ZRAWMAX, -10000, 10000)) / 10000;

}

void taratura(void) {

	scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET);

	for (j = 0; j < 6; j++) {

		printf("TARATURA NUMERO %d\n", j + 1);

		leggiSensori();

		soglia1 = soglia1 + avg1;
		soglia2 = soglia2 + avg2;
		soglia3 = soglia3 + avg3;

		printf("soglia1: %f - soglia2: %f - soglia3: %f\n", soglia1, soglia2,soglia3);

		HAL_Delay(5000);

	}

	soglia1 = (soglia1/6) + OFFSET;
	soglia2 = (soglia2/6) + OFFSET;
	soglia3 = (soglia3/6) + OFFSET;

	if (soglia1 >= MAX) {

		soglia1 = MAXDEFAULT;

	}

	if (soglia2 >= MAX) {

		soglia2 = MAXDEFAULT;

	}

	if (soglia3 >= MAX) {

		soglia3 = MAXDEFAULT;

	}

	printf("soglia1: %f - soglia2: %f - soglia3: %f\n", soglia1, soglia2, soglia3);

	HAL_Delay(5000);

}

void scriviAttuatori(GPIO_PinState red, GPIO_PinState green, GPIO_PinState blue, GPIO_PinState vibration, GPIO_PinState alarm) {

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, red);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, green);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, blue);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, vibration);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, alarm);

}

void controlloPostura(void){

	if (avg1 < soglia1 && avg2 >= soglia2 && avg3 >= soglia3 && tallone == -1 && punta == -1) {

			  fermo = 1;

			  if (inizio == -1) {

				  tempo_tallone=0;
				  inizio = 1;
				  scriviAttuatori(GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET);

			  }

			  if (inizio == 1) {

				  tempo_tallone++;

				  if(tempo_tallone >= CONT) {

					  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_SET);

					  sprintf(echo, "2");

					  wifi_state = wifi_state_socket_write;
				  }

			  }

		  }

		  if (avg1 >= soglia1 && avg2 >= soglia2 && avg3 >= soglia3 && inizio == 1 && punta == -1) {

			  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_RESET);

			  inizio = -1;
			  tallone = -1;

			  fermo = 1;

		  }

		  if (avg1 >= soglia1 && (avg2 < soglia2 || avg3 < soglia3) && inizio == -1 && punta == -1) {

			  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_SET);

			  sprintf(echo, "0");

			  wifi_state = wifi_state_socket_write;

			  fermo = 1;

		  }

		  if (avg1 < soglia1 && avg2 < soglia2 && avg3 < soglia3 && tallone == 1 && punta == 1) {

			  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_RESET);

			  fermo = 1;

		  }

		  if (avg1 < soglia1 && avg2 >= soglia2 && avg3 >= soglia3 && tallone == 1) {

			  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_RESET);

			  tallone = -1;
			  punta = -1;

			  fermo = 1;

		  }

		  if (avg1 < soglia1 && avg2 < soglia2 - SIDE && avg3 >= soglia3) {

			  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_SET);

			  inizio = 1;
			  tallone = -1;
			  punta = -1;

			  sprintf(echo, "3");

			  wifi_state = wifi_state_socket_write;

			  fermo = 1;

		  }

		  if (avg1 < soglia1 && avg2 < soglia2 && avg3 < soglia3 && tallone == -1 && punta == -1 && inizio == 1) {

			  scriviAttuatori(GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_RESET);

			  tallone = 1;

			  fermo = 1;

		  }

		  if (avg1 >= soglia1 && avg2 < soglia2 && avg3 < soglia3 && tallone == 1) {

			  fermo = 1;

			  if (punta == -1) {

				  tempo_punta = 0;
				  punta = 1;
				  inizio = -1;

				  scriviAttuatori(GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET);

			  }

			  if (punta == 1) {

				  tempo_punta++;

				  if (tempo_punta >= CONT) {

					  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_SET);

					  sprintf(echo, "1");

					  wifi_state = wifi_state_socket_write;

				  }

			  }

		  }

		  if (avg1 >= soglia1 && avg2 >= soglia2 && avg3 >= soglia3 && tallone == 1 && punta == 1) {

			  scriviAttuatori(GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_RESET);

			  tallone = -1;
			  punta = -1;
			  inizio = -1;

			  fermo = 0;

		  }

		  if (avg1 < soglia1 && avg2 >= soglia2 && avg3 < soglia3 - SIDE) {

			  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_SET);

			  inizio = 1;
			  tallone = -1;
			  punta = -1;

			  sprintf(echo, "4");

			  wifi_state = wifi_state_socket_write;

			  fermo = 1;

		  }

		  if (avg1 < soglia1 && avg2 < soglia2 && avg3 < soglia3 && inizio == -1 && punta == -1) {

			  scriviAttuatori(GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_RESET);

			  fermo = 1;

		  }

		  if (avg1 >= soglia1 && avg2 >= soglia2 && avg3 >= soglia3 && inizio == -1 && punta == -1) {

			  scriviAttuatori(GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_RESET);

		  }

		  //DESTRA
		  if (yAccel >= AXTHRESHOLD && (-(1 - AXTHRESHOLD) <= zAccel) && (zAccel <= (1 - AXTHRESHOLD)) && avg1 > soglia1 && avg2 > soglia2 && avg3 > soglia3 && fermo == 1) {

			  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_SET);



			  sprintf(echo,"8");
			  wifi_state=wifi_state_socket_write;

		  }
		  //SINISTRA
		  if (yAccel <= -AXTHRESHOLD && (-(1 - AXTHRESHOLD) <= zAccel) && (zAccel <= (1 - AXTHRESHOLD)) && avg1 > soglia1 && avg2 > soglia2 && avg3 > soglia3 && fermo == 1) {

			  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_SET);



			  sprintf(echo,"7");
			  wifi_state=wifi_state_socket_write;

		  }
		  //PUNTA
		  if (xAccel <= -AXTHRESHOLD && (-(1 - AXTHRESHOLD) <= zAccel) && (zAccel <= (1 - AXTHRESHOLD)) && avg1 > soglia1 && avg2 > soglia2 && avg3 > soglia3 && fermo == 1) {

			  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_SET);



			  sprintf(echo,"5");
			  wifi_state=wifi_state_socket_write;

		  }

		  //TALLONE
		  if (xAccel >= AXTHRESHOLD && (-(1 - AXTHRESHOLD) <= zAccel) && (zAccel <= (1 - AXTHRESHOLD)) && avg1 > soglia1 && avg2 > soglia2 && avg3 > soglia3 && fermo == 1) {

			  scriviAttuatori(GPIO_PIN_SET, GPIO_PIN_RESET, GPIO_PIN_RESET, GPIO_PIN_SET, GPIO_PIN_SET);



			  sprintf(echo,"6");
			  wifi_state=wifi_state_socket_write;

		  }

}

/**
  * @}
  */
  
/**
* @}
*/
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
